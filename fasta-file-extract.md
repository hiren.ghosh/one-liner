# Extract fasta sequences from a file using a list in another file.
#### Lets say we have a fasta file (test.fatsa) like: 
    >seq1
    ACTGACAAAGCATGCACGTCATTTT
    >seq2
    ATGCATCAGCATATGACCCCCGTTTA      
    >seq3
    CGTCGAAAAATTTCGATACACCCTAT

#### We would like tp extact fasta sequence by header name 
    
    # install seqtk
    # make a list of header that you would like to extract
    # file should be like this 
    # a.txt 
    seq1
    seq2
    
#### Use Seqtk 
    $seqtk subseq test.fa a.txt > subset.fasta 



