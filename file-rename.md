# Renaming files from a list of filenames

## Create a list of example files using touch 

    touch file- mygenome{1..5}.fasta

## See the order of those file by 

    ls *fasta > mygenome.txt
            
## the file should be like this 
    1.fasta
    2.fasta
    3.fasta
    4.fasta

## Make a new file "name_change.txt" with with new name the order should be same 
### New file should be like this 

    6.fasta
    7.fasta
    8.fasta
    9.fasta


## rename all the file 
               
    for file in *.fasta; do read line;  mv -v "${file}" "${line}";  done < name_change.txt

    renamed '1.fasta' -> '6.fasta'
    renamed '2.fasta' -> '7.fasta'
    renamed '3.fasta' -> '8.fasta'
    renamed '4.fasta' -> '9.fasta'
