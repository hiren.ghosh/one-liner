# Find a list of file with Wildcard and copy 

    find -iname '*.fasta' -exec cp {} /home/sk/test2/ \;
    # Another way to do same task
    find ./test -type f -name "*.log" | xargs -I % mv % backup/

# Find the file path name  
    find -iname '*.fasta' > mygenome.txt
    #create a new_folder 
    cat mygenome.txt | xargs -I % cp % new_folder
